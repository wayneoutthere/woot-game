# An attempted game for Amber and Myla so that they
# can try interracting with software
from yesNoTest import yesNoTester


print('''

|=====================+===================|
| 2019                                    |
|                                         |
| X        X                         X    |
| X        X                         X    |
| XX  X   XX  XXXX    XXXX   XXXXX   X    |
|  X X X X    X  X    X  X     X     X    |
|   X   X     X  X    X  X     X          |
|   X   X     XXXX    XXXX     X     X    |
|                                         |
| a RANDOM choose-your-own-adventure game |
|    for Myla-Styla and Ambidextrous      |
|                                     |
|=========================================|

  ''' )

prompt = '> '

def dead(why):
    print(why, "Game over, try again.")
    exit(0)

def find_treasure():
    print('''
===================================
| You rejoice exceedingly!   What lies in front of you?
| A large chest with the top slightly open which reveals the
| shimmer and glimmer of gold and silver and jewels!
|
| You do a happy dance.
| You do the dab.
| You put your right foot in and your left foot in and
| you shake it all about!
|
| Don't spend your gold all at once and don't forget to
| put some towards your parents' mortgage payments!
===================================
    ''')
    exit(0)

def aboard_boat():
    print('''
===================================================
|You have chosen to climb aboard the boat.
|
|You start to climb the ladder, but as you reach the top
|you hear a shout, "Who goes there!!!?".  It's a scary
|looking pirate with an eye patch, three missing teeth,
|and a really big sword.
|
|He grabs the ladder and pushes it back.
|
|You fall into the ocean.
==================================================
    ''')
    dead("Climbing aboard the boat was a poor choice.")

def cliffs():
    print('''
=================================================
| You stroll for a little longer.  The wind is chilly on
| your face and the smell of salty air again fills your nostrils
|
| As you slice and dice your way through the thick forest brush
| with your trusty sword, you find yourself standing at the top
| of a very steep cliff.
|
| Type 'go' to continue...
===================================
    ''')
    while True:
        go004 = input(prompt)
        if go004 == 'go':
            print('''
=============================================
| Do you:
|
| 1. Climb down this dangerous cliff face to see what's
| down there? Or,
| 2. Go back towards the boat and climb aboard? Or,
| 3. Scream for help and hope someone hears you?
|
| Choose wisely!
===================================
    ''')
        else:
            print("\'go\'is your only option there, bucko!")
            break

        for ch112 in range(1,4):
            ch112 = str(input())

            if ch112 == '1':
                print('''
=========================
| You decide to climb down the cliff ...

                ''')
                find_treasure()

            elif ch112 == '2':
                aboard_boat()

            elif ch112 == '3':
                dead("When will you learn screaming doesn't scare pirates?  They catch you.")

            else:
                print("1, 2, or 3, please.")

print('''
===================================================
| Hello! Welcome to "Woot".
| What's your name?"
===================================================
''')

yourName = input(prompt)

print(f"Thanks for that, {yourName}!")
print("Let's begin a new kind of Peter Hope Lake adventure, ok?")

def gold_coins():
    print('''
===================================================
|    Silence.  Pure silence.  Just the sound of a creak
|    repeating every once in a while.  But what is the creaking sound?
|
|    You realize that you are standing on the bank of a body of water.
|    It's dark, but you can feel the breeze of the ocean and
|    smell the salty air.
|
|    You look up.  You can just barely make out the faint
|    outline of what appears to be a large wooden boat.
|
|    Type 'go' to continue...
===================================================
    ''')

    while True:
        go003 = input(prompt)
        if go003 == 'go':
            print('''
===================================================
|    What is this boat?  Why am I here?  And... more importantly,
|    what in the world should I do now that I'm stuck here alone?
|
|    Do you:
|        1. Climb aboard this strange boat?
|        2. Turn the opposite way and explore more? or
|        3. Scream for help?
===================================================
            ''')
        else:
            print("\'go\' is your only option there, bucko!")
            break

        for ch11 in range(1,4):
            ch11 = str(input())
            if ch11 == "1":
                aboard_boat()

            if ch11 == '2':
                cliffs()

            if ch11 == '3':
                dead("The pirates heard you and found you.  Choosing to scream for help was a poor idea.")

            else:
                print("Please choose 1, 2, or 3.")

def ming():
    print('''
===================================================
| You have entered the Ming Dynasty.  This dynasty is old... and Asian.
| You look around.  Beautiful white and blue Ming vases, bamboo, fine
| cermamic stuff as the whining sound of the guzheng fills the air.
|
| Type 'go' to continue...
===================================================
        ''')

    while True:
        go006 = input("Type 'go' >  ")

        if go006 == 'go':
            print('''
===================================================
| ... then suddenly busting out from the paper-like material of the
| wall in front of you is a ninja - with a big sword (of course).  And..
| screaming really loud to scare you (of course).
|
| Although you may be scared, you remember that one time you did a
| pretty good ninja move in the backyard...
|
| Press go to continue...
===================================================
            ''')
            break
        else:
            print("You have but one choice: 'go'.")

    while True:
        go007 = input("Type 'go > ")

        if go007 == 'go':
            print('''
===================================================
| You have to make a choice and now is your time.  Do you:
|
| 1. Run at the ninja screaming and do a flying sidekick?
| 2. Release your inner chi?
| 3. Run away?
===================================================
            ''')
            break

        else:
            print("One must needs enter 'go' to move forward, righto?")

    for ch2112 in range(1,4):
        ch2112 = input("Choose 1, 2, or 3.")

        if ch2112 == '1':
            print('''
===================================================
| You choose to do a flying side kick but...
| The ninja's skills are shockingly good.
| Completely out of nowhere and rooted in the ancient
| Wu Taylor fighting school comes...
|
| ...The Dancing Leopard move!
===================================================
            ''')
            dead("You were no match for Dancing Leopard...")

        elif ch2112 == '2':
                print('''
===================================================
| You inhale.
| You exhale.
| You inhale again.
| You prepare to release your inner chi to defeat this wicked foe...
| but... you suddenly realize.. there is no such thing as inner chi!
| plus... you had too many onions the night before...
| ... and instead you release a large toot!
| It turns out that 'inner chi' is nothing more than 'inner gas!'
| What a scam!
=================================================
                ''')
                dead("Your foe laughs at you and your inner gas, then defeats you.")

        elif ch2112 == '3':
            print("Run away?  How cowardly!  But.. meh.  Why not?  You run, you run some more and surprise, surprise!")
            find_treasure()


def old_west():
    print('''
===================================================
| Suddenly you are in the Old Wild West in a sandy desert
| of some kind where the eerie sound of a raven cawing and
| the sandy wind blowing by your ears are the only sound.
|
| You hear the click of a gun behind you.
|
| You turn around and there is a cowboy with a gun pointed at you!
|
| You must chose. Do you:
|
| 1. Pull your gun out and match his rude and violent behaviour?
| 2. Do the chicken dance (surely you learned this for a reason)?
| 3. Run away?
===================================================
    ''')
    for ch2111 in range(1,4):
        ch2111 = input("1, 2, or 3? > ")

        if ch2111 == '1':
            print('''
====================================================
| You choose to grab your six shooter and point it at this violent
| cowboy.
|
| ... Unfortunately, brains are typically better than guns and he's
| much faster than you.
====================================================
            ''')
            dead("Better luck next time.")

        elif ch2111 == '2':
            print('''
====================================================
| You put your left foot in.
| You put your right foot in.
| You put your left foot in... and just as you start to shake it,
| the cowboy starts laughing hysterically...
| ... and then he beats you to a pulp.
====================================================
            ''')
            dead("Better learn to dance better.")

        elif ch2111 == '3':
            print("You run, run, run.  Around the corner, here and there and up and down and...and... guess what? ...")
            find_treasure()

        else:
            print("Please choose 1, 2, or 3.")

def past_intro():
    print('''
===================================================
| Welcome to the past.  As you step in through this door, you are
| greeted with two more doors.  One appears to be a tavern shutter-style
| doors and above it says 'The Old West' while the other has several
| difficult Chinese characters which, thankfully you know, translate to
| "The Ming Dynasty."
|
| Do you:
| 1. Enter into The Old West or
| 2. Try the door to The Ming dynasty
===================================================
    ''')
    for ch211 in range(1,3):
        ch211 = input("Choose 1 or 2. >  ")

        if ch211 == '1':
            old_west()

        elif ch211 == '2':
            ming()
def future():
    print('''
===================================================
| Muhahaha!  Sucker!  Don't you know you can't travel to the future?
| That's just in the movies!
===================================================
    ''')
    dead("I guess we better enjoy the present more!")



def ancient_futures():
    print('''
===================================================
| You have selected option 2 - Ancient Futures
|
| What a mysterious name.  Probably the only reason you chose
| this option, is because it's irresistably mysterious.  How
| could something be both 'ancient' and 'future' at the same time?
|
| Well, that's just it.  In here, nothing is what it seems...
|
| Type 'go' to continue.
===================================================
    ''')

    while True:
        go005 = input("Type 'go' to continue. > ")
        if go005 == 'go':
            print('''
===================================================
| You take a few steps into this strange room.  Like the first
| room when you entered the cabin, the light is dim.  You see
| two doors this time.  One is a beaten up, thick wooden door,
| while the other is a shiny glimmering metallic finish - much like
| stainless steel.
|
| On the wooden door - door number 1 -  reads "To the Past" and
| the metallic door - door number 2 reads "To the Future."
|
| Do you:
|     1. Go through door number 1 or
|     2. Go through door number 2?
====================================================
            ''')
            break
        else:
            print("\'go\' is your only option there, bucko!")

    for ch21 in range(1,3):
        ch21 = input("1 or 2? >  ")

        if ch21 == '1':
            past_intro()

        elif ch21 == '2':
            future()

        else:
            print("Please choose either 1 or 2.")


def start():
    print('''

====================================================
|    Read this.  If you can't read or need help, get someone older
|    to read with
|
|
|    You are walking towards the cabin.  The stars are out, and so is the moon.
|    even though it\'s the middle of summer, this night is chillier than usual.
|
|    Just like normal, you turn the handle on the door of the cabin.
|    Just as always, the gentle creeeeeak of the handle comes out.
|
|    As you slowly open the door, you realize that something is not quite the
|    same as always.  Instead of being greeted with the smooth wooden staircase,
|    instead you walk into a big, hollow room.
|
|    Type 'go' to continue....
===================================================
''')

start()

while True:
    go001 = str(input(prompt))

    if go001 == 'go':
        print("""| You rejoice exceedin================

===================================================
|    SLAM!
|
|    The door closes behind you.  You try to open it, but it's locked.
|
|    The moonlight shines in through a high window and in front of you
|    appear two doors.
|
|    As you approach the doors, the moonlight hits the door on the left.
|    You get closer to read it.
|    "Gold Coins"
|
|    On the right door, there is dust covering the sign.  You blow it off.
|    "Ancient Future" it reads.
|
|    Type 'go' to continue...
===================================================
""")
        break

    else:
        print("\'go\' is your only option there, bucko!")

while True:
    go002 = input(prompt)

    if go002 == 'go':
        print("""
===================================================
|    With no way back, you must enter through one of the doors.
|
|    Which door will it be?
|
|    Door #1, Gold Coins? or ...
|    Door #2, Ancient Future?
|
|    Choose a number... And choose wisely!
===================================================
""")
        break
    else:
        print("\'go\' is your only option there, bucko!")

for ch00 in range (1, 3):
    ch00 = input("> ")

    if ch00 == '1':
        print('You have selected door #1: Gold Coins.')
        gold_coins()
        break

    elif ch00 == '2':
        print('You have selected door #2.')
        ancient_futures()
        break

    else:
        print("Try again, please - just 1 or 2 please.")
